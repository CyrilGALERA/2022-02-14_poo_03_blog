<?php
/* => here */

class Database
{

    private $db; // private class's member (attribute, property, variable)

    // constants in class .. How to use it ? 
    // http://www.lephpfacile.com/manuel-php/language.oop5.constants.php
    const DB_NAME = 'simplonDatabase';
    const DB_HOST = 'localhost';
    const DB_USER = 'u_cyril';
    const DB_PASSWORD = 'pwd_cyril';
    const DB_TABLE = 'posts';

    public function __construct()
    {
        $this->connectbdd(); // store the connexion in $db
    }

    // private => can be called only here from THIS script !!
    private function connectbdd()
    {
        try {
            $this->db = new PDO('mysql:host=' . self::DB_HOST . ';dbname=' . self::DB_NAME . ';charset=utf8', self::DB_USER, self::DB_PASSWORD, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        } catch (Exception $e) {
            echo '<div class="alert alert-danger" role="alert">
                    ' . $e->getMessage() . '
                </div>';
            //die('Erreur : ' . $e->getMessage());
        }
    }

    // public $db's getter
    public function getDB()
    {
        return $this->db;
    }

    /***********************************************/
    /*                  POSTS CRUD                 */
    /***********************************************/
    //public or private here ? please ... put some details !
    function getAllPosts()
    {
        $sql = 'SELECT * FROM ' . self::DB_TABLE;
        $req = $this->db->query($sql);
        $result = $req->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }

    public function insertPost($title = "titre par défaut", $content = "contenu par défaut")
    {
        $content1 =htmlentities(
            $content, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML401, null, true
        );
        $content2 =htmlentities(
            $content, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML401, null, true
        );
        $content3 =htmlentities(
            $content, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML401, null, true
        );

        echo "</p> Create Post $title $content2 : ";
        $sql = 'INSERT INTO ' . self::DB_TABLE . ' (title, content) VALUES (:title, :content2)';
        echo "</p> sql = $sql";
        $req = $this->db->prepare($sql);
        try {
            $result = $req->execute([
                'title' => $title,
                'content2' => $content2
            ]);
            return $result;
        } catch (PDOException $e) {
            return 'error: ' . $e->getMessage();
        }
    }

    /******** HERE ==> YOUR MISSION FOR TODAY ******/
    /***********************************************/
    function updatePost($idPost, $title = "titre MAJ par défaut", $content = "contenu MAJ par défaut")
    {
        $sql = 'UPDATE ' . self::DB_TABLE.' SET title = :title, content = :content WHERE id = :idPost';
        $req = $this->db->prepare($sql); //already prepared $req
        //execute the $req with parameters
        //keep try/catch for bot's validation
        try {
            $result = $req->execute([
                'title' => $title,
                'content' => $content,
                'idPost' => $idPost
            ]);
            return 'OK';
        } catch (PDOException $e) {
            return 'ERROR: ' . $e->getMessage();
        } catch (ParseError $e) {
            return 'ERROR: ' . $e->getMessage();
        }
    }

    /******** HERE ==> YOUR MISSION FOR TODAY ******/
    /***********************************************/
    function getPost($idPost)
    {
        //write your SQL code
        $sql ='SELECT * FROM ' . self::DB_TABLE .' WHERE id = :idPost';
        $req = $this->db->prepare($sql); //already prepared $req

        //execute the $req with parameters
        //keep try/catch for bot's validation
        try {
            $result = $req->execute([ 'idPost' => $idPost ]);
            $result = $req->fetchAll(PDO::FETCH_OBJ);
            return $result[0];
        } catch (PDOException $e) {
            return 'ERROR: ' . $e->getMessage();
        } catch (ParseError $e) {
            return 'ERROR: ' . $e->getMessage();
        }
    }

    /******** HERE ==> YOUR MISSION FOR TODAY ******/
    /***********************************************/
    function deletePost($idPost)
    {
        $sql = 'DELETE FROM ' . self::DB_TABLE.' WHERE id = :idPost';
        $req = $this->db->prepare($sql); //already prepared $req
        //execute the $req with parameters
        //keep try/catch for bot's validation
        try {
            $req->execute([
                'idPost' => $idPost ]);
            return 'OK';
        } catch (PDOException $e) {
            return 'ERROR: ' . $e->getMessage();
        } catch (ParseError $e) {
            return 'ERROR: ' . $e->getMessage();
        }
    }
}

?>